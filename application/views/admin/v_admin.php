<!DOCTYPE html>
<html>
<head>
	<title>Data Admin</title>
</head>
<body>
<h1>Data Admin</h1>
	<table border="1">
		<tr>
			<td>ID</td>
			<td>Nama</td>
			<td>Username</td>
			<td>Password</td>
			<td>Nomor Telepon</td>
		</tr>
		<?php foreach ($content as $key): ?>
			<tr>
				<td><?php echo $key->admin_id ?></td>
				<td><?php echo $key->admin_name ?></td>
				<td><?php echo $key->admin_login ?></td>
				<td><?php echo $key->admin_password ?></td>
				<td><?php echo $key->admin_phone ?></td>
				<td><a href=<?php echo "edit/".$key->admin_id; ?>>edit</a></td>
				<td><a href=<?php echo "delete/".$key->admin_id; ?>>delete</a></td>
			</tr>
		<?php endforeach ?>
	</table>
</body>
</html>