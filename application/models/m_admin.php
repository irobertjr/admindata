<?php
class M_admin extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	} 

    function selectAll()
   {
        $this->db->order_by("admin_id","desc"); 
        return $this->db->get('admins')->result();
   }

   public function m_delete($id)
   {
       $this->db->delete('admins',array('admin_id'=>$id));
   }

   public function get_data_edit($id)
   {
       $query = $this->db->get_where('admins',array('admin_id'=>$id));
       return $query->row_array();
   }

   public function m_edit($id)
   {
        $data = array(
                'admin_name'=> $this->input->post('nama'),
                'admin_login'=>$this->input->post('username'),
                'admin_password'=>$this->input->post('password'),
                'admin_phone'=>$this->input->post('phone')

            );
        $this->db->where('admin_id',$id);
        return $this->db->update('admins',$data);
   }

}
?>