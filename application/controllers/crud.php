<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class crud extends CI_controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('m_admin');
		$this->load->helper(array('url','form'));
	}

	public function index()
	{
		$data['content']=$this->m_admin->selectAll();
		$this->load->view('admin/v_admin',$data); //lokasi view
	}

	public function update($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama','Nama', 'required');
		$this->form_validation->set_rules('username','username', 'required');
		$this->form_validation->set_rules('password','password', 'required');
		$this->form_validation->set_rules('phone','phone', 'required');

		if($this->form_validation->run()===FALSE)
		{

			$data['get_data']=$this->m_admin->get_data_edit($id);
			$this->load->view('admin/v_edit',$data);

		}else{
			$this->m_admin->m_edit($id);
			redirect('index.php/dataadmin');
		}
	}

	public function delete($id)
	{
		$this->m_admin->m_delete($id);
		redirect('index.php/dataadmin');
	}

	    function getall() {
        $ambildata = $this->db->get('admins');
        //jika data ada (lebih dari 0)
        if ($ambildata->num_rows() > 0 ) {
            foreach ($ambildata->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

}
 ?>